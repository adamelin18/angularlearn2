import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {ArscheduleModule} from './arschedule/arschedule.module';
import {ScheduleRootComponent} from './arschedule/schedule-root/schedule-root.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ArscheduleModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
