export interface IPatient {
  id: number;
  name: string;
  birthdate: string;
  OMS: string;
}
