export interface ITimeRange {
  type: string;
  time: Array<string>;
  days: string;
}
export interface IAppointment {
  patient: number;
  datetime: string;
}
export interface IDoctor {
  id: string;
  speciality: string;
  name: string;
  cabinet: string;
  scheduleStep: number;
  worktime: Array<string>;
  workdays: string;
  timeranges: Array<ITimeRange>;
  appointments: Array<IAppointment>;
}
export interface IDoctorsCategory {
  category_name: string;
  doctors: Array<IDoctor>;
}
