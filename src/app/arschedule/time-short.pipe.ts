import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeShort'
})
export class TimeShortPipe implements PipeTransform {

  transform(date: Date, format: string): any {
    return moment(date).format(format);
  }

}
