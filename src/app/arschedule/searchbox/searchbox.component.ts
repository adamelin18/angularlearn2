import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm, FormsModule} from '@angular/forms';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {
  @Input() table: Array<object>;
  @Input() searchFields: Array<string>;
  @Input() displayFields: Array<string>;
  @Output() elementSelected: EventEmitter<object> = new EventEmitter();
  public outputTable: Array<any>;
  public ResultsListEnabled: boolean;
  public emitSelected(selected) {
    this.elementSelected.emit(selected);
    this.ResultsListEnabled = false;
  }
  public search(SearchString: NgForm) {
    const SearchValue: string = SearchString.form.value.SearchValue;
    if (SearchValue.length > 3) {
      this.outputTable = this.table.filter((elem) => {
        return this.searchFields.find((fieldName: string) =>
          elem[fieldName].substring(0, SearchValue.length).toLowerCase() === SearchValue.toLowerCase());
      });
      this.ResultsListEnabled = true;
    } else {
      this.ResultsListEnabled = false;
    }
  }
  constructor() {
    this.ResultsListEnabled = false;
  }
  ngOnInit() {}
}
