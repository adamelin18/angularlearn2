import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() setDaysToDisplay: EventEmitter<number> = new EventEmitter();
  public daysToDisplay = 1;
  public buttonsConstructor: Array<number> = [1, 2, 7];
  public getDayCaption(day) {
    switch (day) {
      case 1:
        return '1 день';
      case 2:
        return '2 дня';
      case 7:
        return 'Неделя';
    }
  }
  public daysToDisplayClicked(days: number) {
    this.setDaysToDisplay.emit(days);
    this.daysToDisplay = days;
  }
  constructor() { }

  ngOnInit() {
  }

}
