import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild, ViewContainerRef} from '@angular/core';
import {IDoctor} from '../doctor';
import {IPatient} from '../patient';
import {Subject} from 'rxjs/Subject';
import {IFormatted, IRangeContainer} from '../formatted';
import {IButtonContainer, modalOptions} from '../utils';
import * as moment from 'moment';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css'],
})

export class ScheduleComponent implements OnInit, OnChanges {
  @Input() doctorsFormatted: Array<IFormatted>;
  @Input() isDoctorsSelected: boolean;
  @Input() selectedDate: Date;
  @Input() patients: Array<IPatient>;
  @Input() patientSelected: IPatient;
  @Output() setDaysToDisplay: EventEmitter<number> = new EventEmitter();
  @Output() setAppointment: EventEmitter<object> = new EventEmitter();
  @Output() appointmentDeleted: EventEmitter<object> = new EventEmitter();
  @ViewChild('modalSuccess') modalSuccess: ViewContainerRef;
  @ViewChild('modalConfirmation') modalConfirmation: ViewContainerRef;
  public modalCommand = new Subject();
  public modalData;
  public modalButtonsOptions: Array<IButtonContainer>;
  private selectedRange: IRangeContainer;
  private timer;


  public openToolTip(toolTip) {
    this.timer = setTimeout(() => {
      toolTip.open();
    }, 1000);
  }

  public closeToolTip(toolTip) {
    clearTimeout(this.timer);
    toolTip.close();
  }

  private isPast(dateTime: Date) {
    const currentDateTime = new Date();
    return dateTime < currentDateTime;
  }

  public sendCommand(range, doctor, appIndex) {
    this.selectedRange = {range: range, doctor: doctor, index: appIndex};
    const isUniquePatient =
      (range.appointments.length === 1 && range.appointments[0].patientID !== this.patientSelected.id) ||
      range.appointments.length === 0;
    this.modalButtonsOptions = [
      {
        name: modalOptions.details,
        enabled: range.appointments.length > 0
      },
      {
        name: modalOptions.create,
        enabled:
          range.appointments.length < 2 &&
          !range.blocked &&
          !!this.patientSelected &&
          !this.isPast(range.start) &&
          isUniquePatient
      },
      {
        name: modalOptions.delete,
        enabled: range.appointments.length > 0
      }
    ];
    this.modalData = {};
    if (range.appointments.length > 0) {
      this.modalData.header = this.getPatientCaptionById(range.appointments[appIndex].patientID);
    } else {
      this.modalData.header = `Выбран интервал времени ${this.formatTimeShort(range.start)} - ${this.formatTimeShort(range.end)}`;
    }
    this.modalCommand.next();
  }

  private formatTimeShort(date: Date) {
    return moment(date.getTime(), 'x').format('HH:mm');
  }

  public formatDate(date: Date) {

    const options: Intl.DateTimeFormatOptions = {weekday: 'short', day: 'numeric', month: 'short'};
    const formatter =  new Intl.DateTimeFormat('ru', options);
    return formatter.format(date);
    // return moment(date).format('DD.MM.YYYY');
  }

  private typeVocab(type: string) {
    switch (type) {
      case 'available':
        return 'Запись на прием';
      case 'notWorking':
        return 'Врач не работает';
      case 'vacation':
        return 'Отпуск';
      case 'learning':
        return 'Обучение';
      case 'paperwork':
        return 'Работа с документами';
      case 'home':
        return 'Прием на дому';
      case 'sickLeave':
        return 'Больничный';
    }
  }

  public generateDescription (doctor: IDoctor, date: Date) {
    let desc = doctor.worktime[0];
    doctor.timeranges
      .filter(range => range.days.split(',').indexOf('' + date.getDay()) > -1)
      .forEach(range => {
      if (range.type !== 'available') {
        range.time.forEach(time => {
          desc += '<br>';
          desc += `${this.typeVocab(range.type)} (${time})`;
        });
      }
    });
    return desc;
  }

  public getPatientCaptionById(id: number) {
    const patient = this.patients.find(pat => pat.id === id);
    const nameList = patient.name.split(' ');
    let caption = '';
    for (let i = 0; i < nameList.length; i++) {
      if (i === 0) {
        caption += `${nameList[i]} `;
      } else {
        caption += `${nameList[i].substring(0, 1)}.`;
      }
    }
    return caption;
  }

  public passDays(event: number) {
    this.setDaysToDisplay.emit(event);
  }

  public appointmentSelected(option: modalOptions) {
    switch (option) {
      case modalOptions.details:
        this.modalData = {};
        this.modalData.header = this.getPatientCaptionById(this.selectedRange.range.appointments[this.selectedRange.index].patientID);
        const detailsDate = this.selectedRange.range.start;
        const detailsDoc = this.selectedRange.doctor.name;
        const detailsCabinet = this.selectedRange.doctor.cabinet;
        const detailsOMS = this.patients.find((patient: IPatient) => patient.id === this.selectedRange.range.appointments[this.selectedRange.index].patientID).OMS;
        this.modalData.details = `Дата: ${detailsDate}<br>Врач: ${detailsDoc}<br>Кабинет: ${detailsCabinet}<br>Полис ОМС: ${detailsOMS}`;
        this.modalCommand.next();
        break;
      case modalOptions.create:
        const appointmentToSet = {dateTime: this.selectedRange.range.start, doctorId: this.selectedRange.doctor.id};
        this.setAppointment.emit(appointmentToSet);
        this.openSuccessModal();
        break;
      case modalOptions.delete:
        this.openConfirmationModal();
        break;
      case modalOptions.showOptions:
        this.sendCommand(this.selectedRange.range, this.selectedRange.doctor.id, this.selectedRange.index);
    }
  }

  private openSuccessModal() {
    const successModalRef = this.modalService.open(this.modalSuccess);
    const timer = setTimeout(() => successModalRef.close(), 3000);
    successModalRef.result.then(() => {
      clearTimeout(timer);
      }, () => {
      clearTimeout(timer);
    });
  }

  private openConfirmationModal() {
    const confirmModalRef = this.modalService.open(this.modalConfirmation);
    confirmModalRef.result.then(() => {
      this.deleteAppointment();
    }, () => {});
  }

  private deleteAppointment() {
    const appointmentToDelete = {
      dateTime: this.selectedRange.range.start,
      doctorId: this.selectedRange.doctor.id,
      patientId: this.selectedRange.range.appointments[this.selectedRange.index].patientID
    };
    this.appointmentDeleted.emit(appointmentToDelete);
  }
  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
  }
}
