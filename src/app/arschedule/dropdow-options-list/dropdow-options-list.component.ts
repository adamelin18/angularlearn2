import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-dropdown-options-list',
  templateUrl: './dropdow-options-list.component.html',
  styleUrls: ['./dropdow-options-list.component.css']
})
export class DropdowOptionsListComponent implements OnInit {
  @ViewChild('DropdownContainer') DropdownContainer: ElementRef ;
  @ViewChild('DropdownButton') DropdownButton: ElementRef ;
  @Input() OptionsNames: Array<string>;
  @Input() Disabled: boolean;
  @Output() selectedOption: EventEmitter<string> = new EventEmitter();
  public DropdownEnabled: boolean;
  @HostListener('document:click', ['$event']) bgClick(event: Event) {
    if (!this.DropdownContainer.nativeElement.contains(event.target) && event.target !== this.DropdownButton.nativeElement && this.DropdownEnabled) {
      this.ToggleDropdownVisibility();
    }
  }
  public ToggleDropdownVisibility(): void {
    this.DropdownEnabled = !this.DropdownEnabled;
  }
  public emitSelected(option) {
    this.selectedOption.emit(option);
    this.ToggleDropdownVisibility();
  }
  constructor() {
    this.DropdownEnabled = false;
  }
  ngOnInit() {
  }
}
