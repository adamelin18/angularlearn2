import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdowOptionsListComponent } from './dropdow-options-list.component';

describe('DropdowOptionsListComponent', () => {
  let component: DropdowOptionsListComponent;
  let fixture: ComponentFixture<DropdowOptionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdowOptionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdowOptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
