import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {IButtonContainer, modalOptions} from '../utils';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {
  @ViewChild('content') content: ElementRef ;
  @Input() command: Observable<any>;
  @Input() details;
  @Input() modalButtonsOptions: Array<IButtonContainer>;
  @Output() confirmed: EventEmitter<string> = new EventEmitter();
  private subscription: Subscription;
  public buttonsBuilder: Array<string> = Object.values(modalOptions);
  public open() {
    this.modalService.open(this.content).result.then((result) => {
      this.confirmed.emit(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    if (this.command) {
      this.subscription = this.command.subscribe(() => {
        this.open();
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
