import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleRootComponent } from './schedule-root.component';

describe('ScheduleRootComponent', () => {
  let component: ScheduleRootComponent;
  let fixture: ComponentFixture<ScheduleRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
