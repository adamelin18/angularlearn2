import {Component, OnDestroy, OnInit} from '@angular/core';
import {IPatient} from '../patient';
import {IDoctor} from '../doctor';
import {Subscription} from 'rxjs/Subscription';
import {HttpClient} from '@angular/common/http';
import {DataTransformatorService} from '../data-transformator.service';
import {IFormatted} from '../formatted';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-schedule-root',
  templateUrl: './schedule-root.component.html',
  styleUrls: ['./schedule-root.component.css'],
  providers: [DataTransformatorService]
})
export class ScheduleRootComponent implements OnInit, OnDestroy {
  public patients: Array<IPatient>;
  public doctors: BehaviorSubject<Array<IDoctor>> = new BehaviorSubject([]);
  public SelectedDoctors: BehaviorSubject<Array<IDoctor>> = new BehaviorSubject([]);
  public SelectedPatient: IPatient;
  public daysToDisplay: BehaviorSubject<number> = new BehaviorSubject(1);
  public SelectedDate: BehaviorSubject<Date> = new BehaviorSubject(new Date());
  public doctorsFormatted: Array<IFormatted>;
  private subscription: Subscription;

  public setAppointment(appointmentContainer) {
    if (this.SelectedPatient) {
      appointmentContainer.patient = this.SelectedPatient.id;
      this.dataTransformatorService.addAppointment(this.doctors.getValue(), appointmentContainer);
      this.doctors.next(this.doctors.getValue());
    }
  }

  public setDaysToDisplay(days: number) {
    this.daysToDisplay.next(days);
  }

  public selectPatient(patient: IPatient) {
    this.SelectedPatient = patient;
  }

  public selectDoctors(selectedDoctors: Array<IDoctor>) {
    this.SelectedDoctors.next(selectedDoctors);
  }

  public selectDate(date: Date) {
    this.SelectedDate.next(date);
  }

  public deleteAppointment(appContainer) {
    this.dataTransformatorService.deleteAppointmens(this.doctors.getValue(), appContainer);
    this.doctors.next(this.doctors.getValue());
  }

  constructor(private http: HttpClient, private dataTransformatorService: DataTransformatorService) {
    this.SelectedPatient = null;
    this.subscription = Observable.combineLatest(this.SelectedDoctors, this.SelectedDate, this.daysToDisplay, this.doctors)
      .map(([doctors, date, daysToDisplay]: [Array<IDoctor>, Date, number]) =>
        this.dataTransformatorService.updateDoctorsSchedule(doctors, date, daysToDisplay))
      .subscribe(doctorsFormatted => this.doctorsFormatted = doctorsFormatted);
  }

  ngOnInit() {
    this.http.get('assets/patients.json').subscribe((data: Array<IPatient>) => {
      this.patients = data;
    });
    this.http.get('assets/doctors.json').subscribe((data: Array<IDoctor>) => {
      data.sort((a: IDoctor, b: IDoctor) => {
        return a.name > b.name ? 1 : -1;
      });
      this.doctors.next(data);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
