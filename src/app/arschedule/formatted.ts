import {IDoctor} from './doctor';

export interface IAppointmentFormatted {
  patientID: number;
}

export interface IFormattedRange {
  start: Date;
  end: Date;
  type: string;
  blocked: boolean;
  appointments: Array<IAppointmentFormatted>;
}

export interface IFormatted {
  doctor: IDoctor;
  timeRanges: Array<IFormattedRange>;
}
export interface IRangeContainer {
  doctor: IDoctor;
  range: IFormattedRange;
  index: number;
}
