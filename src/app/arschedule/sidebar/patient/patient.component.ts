import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IPatient} from '../../patient';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  @Input() patients;
  @Output()PatientSelection: EventEmitter<IPatient> = new EventEmitter();
  public SelectedPatient: IPatient;
  private emitPatient(patient: IPatient) {
    this.PatientSelection.emit(patient);
  }
  public deselectPatient() {
    this.SelectedPatient = null;
    this.emitPatient(null);
  }
  public selectPatient(patient: IPatient) {
    this.SelectedPatient = patient;
    this.emitPatient(patient);
  }
  constructor() {
  }
  ngOnInit() {
  }

}
