import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IDoctor, IDoctorsCategory} from '../../doctor';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {
  @Input() doctors: Array<IDoctor>;
  @Output() doctorsSelection: EventEmitter<Array<IDoctor>> = new EventEmitter();
  public listStructure: string;
  public selectedDoctors: Array<IDoctor>;
  public doctorsCheckList: FormGroup;
  public doctorsCategorized: Array<IDoctorsCategory>;
  private formConfig: object = {};
  private checkListSubscription: Subscription;
  private subscribeToCheckList() {
    this.checkListSubscription = this.doctorsCheckList.valueChanges.subscribe(() => this.updateDoctorsSelection());
  }
  private updateDoctorsSelection() {
    this.selectedDoctors = this.doctors.filter(doctor => {
      return this.doctorsCheckList.value[doctor.id];
    });
    this.doctorsSelection.emit(this.selectedDoctors);
  }
  private createFormConfig() {
    this.doctors.forEach((doctor: IDoctor) => {
      this.formConfig[doctor.id] = false;
    });
  }
  private patchFormElement(key: string, value: any) {
    const TempVal: object = {};
    TempVal[key] = value;
    this.doctorsCheckList.patchValue(TempVal);
  }
  private selectAll() {
    this.doctors.forEach(doctor => {
      if (this.doctorsCheckList.value[doctor.id] === false) {
        this.patchFormElement(doctor.id, true);
      }
    });
  }
  private deselectAll() {
    this.doctors.forEach(doctor => {
      if (this.doctorsCheckList.value[doctor.id] === true) {
        this.patchFormElement(doctor.id, false);
      }
    });
  }
  private categorizeFormDoctors() {
    const categories = [];
    this.doctors.forEach(doctor => {
      if (categories.indexOf(doctor.speciality) === -1) {
        categories.push(doctor.speciality);
      }
    });
    const categorized = categories.map((category: string) => {
      const category_name: string = category;
      const doctors: Array<IDoctor> = this.doctors.filter((doctor: IDoctor) => {
        return doctor.speciality === category;
      });
      return {category_name: category_name, doctors: doctors};
    });
    return categorized;
  }
  private scrollToElement(divId: string) {
    const elementToDisplay = document.getElementById(divId);
    elementToDisplay.offsetParent.scrollTop = elementToDisplay.offsetTop;
  }
  public handleDropDownOptions(option: string) {
    if (option === 'Выбрать все') {
      this.selectAll();
    } else if (option === 'Отменить все выбранные') {
      this.deselectAll();
    }
  }
  public changeStructure(type) {
    this.listStructure = type;
  }
  public doctorFound(doctor: IDoctor) {
    this.scrollToElement('doctorBlock' + doctor.id);
    const TempVal: object = {};
    TempVal[doctor.id] = true;
    this.doctorsCheckList.patchValue(TempVal);
    this.patchFormElement(doctor.id, true);
  }
  public toggleCategoryState(category: IDoctorsCategory, event: any) {
    category.doctors.forEach((doctor: IDoctor) => {
      this.patchFormElement(doctor.id, event.target.checked);
    });
  }
  constructor(private fb: FormBuilder) {
    this.listStructure = 'ab';
    this.selectedDoctors = [];
  }
  ngOnInit() {
    this.doctorsCategorized = this.categorizeFormDoctors();
    this.createFormConfig();
    this.doctorsCheckList = this.fb.group(this.formConfig);
    this.subscribeToCheckList();
  }
}
