import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {IPatient} from '../patient';
import {IDoctor} from '../doctor';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() patients: IPatient;
  @Input() doctors: IDoctor;
  @Input() SelectedDoctors: Array<IDoctor>;
  @Output() PatientSelection: EventEmitter<IPatient> = new EventEmitter();
  @Output() DoctorsSelection: EventEmitter<Array<IDoctor>> = new EventEmitter();
  @Output() DateSelection: EventEmitter<Date> = new EventEmitter();
  public passPatient(patient: IPatient) {
    this.PatientSelection.emit(patient);
  }
  public passDoctors(doctorsSelected: Array<IDoctor>) {
    this.DoctorsSelection.emit(doctorsSelected);
  }
  public passDate(date: Date) {
    this.DateSelection.emit(date);
  }
  constructor() {}
  ngOnInit() {
  }

}
