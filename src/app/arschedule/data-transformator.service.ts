import {IAppointment, IDoctor, ITimeRange} from './doctor';
import * as moment from 'moment';
import {IFormatted, IFormattedRange} from './formatted';
import {IBaseRange} from './utils';

export class DataTransformatorService {
  private readonly minute = 60 * 1000;
  public addAppointment(doctors: Array<IDoctor>, appointmentContainer): void {
    const doc = doctors.find((doctor: IDoctor) => {
        return doctor.id === appointmentContainer.doctorId;
    });
    const dateTime = moment(appointmentContainer.dateTime.getTime(), 'x').format('DD.MM.YYYY HH:mm');
    const appointment: IAppointment = {patient: appointmentContainer.patient, datetime: dateTime};
    doc.appointments = doc.appointments.concat(appointment);
  }

  public deleteAppointmens(doctors: Array<IDoctor>, appointmentContainer): void {
    const dateTime = moment(appointmentContainer.dateTime.getTime(), 'x').format('DD.MM.YYYY HH:mm');
    const doc = doctors.find((doctor: IDoctor) => {
      return doctor.id === appointmentContainer.doctorId;
    });
    const appToDelete = doc.appointments.find(app => {
      return app.datetime === dateTime && app.patient === appointmentContainer.patientId;
    });
    doc.appointments.splice(doc.appointments.indexOf(appToDelete), 1);
  }

  private sliceDay(doctor: IDoctor, date: Date): Array<Date> {
    const ranges = this.periodToDateFormat(doctor.worktime[0], date);
    const result_ranges: Array<Date> = [];
    while (ranges[0] < ranges[1]) {
      const temp_date = new Date();
      temp_date.setTime(ranges[0].getTime());
      result_ranges.push(temp_date);
      ranges[0].setTime(ranges[0].getTime() + doctor.scheduleStep * this.minute);
    }
    return result_ranges;
  }

  private periodToDateFormat(period: string, date: Date): Array<Date> {
      return period.split('-').map((point: string) => {
        const pointVal = point.split(':');
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), Number(pointVal[0]), Number(pointVal[1]));
      });
  }

  private filterIntervalsAvailableTime(slicedDay: Array<Date>, doctor: IDoctor): Array<Date> {
    const availableRanges: ITimeRange = doctor.timeranges.find(range => range.type === 'available');
    const slicedDayFiltered = slicedDay.filter(interval => {
      const result = availableRanges.time.some(currentRange => {
        const currentRangeDates: Array<Date> = this.periodToDateFormat(currentRange, slicedDay[0]);
        return interval < currentRangeDates[1] && interval >= currentRangeDates[0];
      });
      return result;
    });
    return slicedDayFiltered;
  }

  private addDescription(slicedFilteredDay: Array<Date>, step: number): Array<IFormattedRange> {
    return slicedFilteredDay.map(range =>  {
      const end = new Date();
      end.setTime(range.getTime() + step * this.minute);
      return {start: range, end: end, type: 'available', blocked: false, appointments: []};
    });
  }

  private getDoctorsFormattedDay(doctorsSelected: Array<IDoctor>, displayDate: Date): Array<IFormatted> {
    if (doctorsSelected.length > 0  && displayDate) {
      const result = doctorsSelected
        .filter((doctor: IDoctor) => {
          return doctor.workdays.split(',').indexOf('' + displayDate.getDay()) !== -1; })
        .map((doctor: IDoctor) => {
          const tempRangesTable = this.addDescription(this.filterIntervalsAvailableTime(this.sliceDay(doctor, displayDate), doctor), doctor.scheduleStep);
          doctor.timeranges.forEach(timerange => {
            if (timerange.days.split(',').map(string => +string).indexOf(displayDate.getDay()) !== -1
              && timerange.type !== 'available') {
              timerange.time.forEach(range => {
                this.insertPeriod(tempRangesTable, range, timerange.type, doctor.scheduleStep);
              });
            }
          });
          doctor.appointments.forEach((app: IAppointment) => {
            const datetime = app.datetime.split(' ');
            const date = datetime[0].split('.');
            const time = datetime[1].split(':');
            const dateFormatted = new Date(+date[2], +date[1] - 1, +date[0], +time[0], +time[1]);
            this.insertAppointment(tempRangesTable, dateFormatted, app.patient, doctor.scheduleStep);
          });
          return {doctor: doctor, timeRanges: tempRangesTable};
        });
      return result;
    } else {
      return null;
    }
  }

  private getRangesCrossing(X: IBaseRange, Y: IBaseRange) {
    const dayInMilliseconds = 86400000;
    if (Y.end < X.start || X.end < Y.start) {
      return 0;
    } else if (X.start < Y.start && X.end < Y.end) {
      return X.end - Y.start;
    } else if (X.start <= Y.start && X.end >= Y.end) {
      return dayInMilliseconds;
      // возвращает число, заведомо большее любого интервала записи (сутки)
    } else if (Y.start < X.start && Y.end < X.end) {
      return Y.end - X.start;
    } else if (Y.start <= X.start && Y.end >= X.end) {
      return X.end - X.start;
    }
    return 0;
  }

  private insertPeriod(slicedFilteredDayDescription, range: string, type: string, step: number): void {
    const rangeDateFormat = this.periodToDateFormat(range, slicedFilteredDayDescription[0].start);
    const startAnchor = slicedFilteredDayDescription.find((slice) => {
      return slice.start >= rangeDateFormat[0];
    });
    const startIndex = slicedFilteredDayDescription.indexOf(startAnchor);
    const endAnchor = slicedFilteredDayDescription.find((slice) => {
      return slice.start >= rangeDateFormat[1] || slicedFilteredDayDescription.indexOf(slice) + 1 === slicedFilteredDayDescription.length;
    });
    const endIndex = slicedFilteredDayDescription.indexOf(endAnchor);
    for (let i = startIndex; i <= endIndex; i++) {
      if (slicedFilteredDayDescription[i]) {
        const sliceCoordinates = {
          start: slicedFilteredDayDescription[i].start.getTime(),
          end: slicedFilteredDayDescription[i].end.getTime()
        };
        const rangeCoordinates = {
          start: rangeDateFormat[0].getTime(),
          end: rangeDateFormat[1].getTime()
        };
        if (this.getRangesCrossing(sliceCoordinates, rangeCoordinates) / (step * this.minute) > 0.2) {
          slicedFilteredDayDescription.splice(i, 1);
          i--;
        }
      }
    }
    slicedFilteredDayDescription.splice(startIndex, 0, {start: rangeDateFormat[0], end: rangeDateFormat[1], type: type});

  }

  private insertAppointment(slicedFilteredDayDescriptionPeriods, appStart: Date, patientID: number, step): void {
    const availableRange = slicedFilteredDayDescriptionPeriods.find(range => range.start.getTime() === appStart.getTime() && range.type === 'available');
    if (availableRange) {
      availableRange.appointments.push({patientID: patientID});
      availableRange.blocked = false;
    } else {
      const rangeToInsertForce = slicedFilteredDayDescriptionPeriods.find(range => {
        return range.type !== 'available' && appStart >= range.start && appStart <= range.end;
      });
      const appEnd = new Date();
      appEnd.setTime(appStart.getTime() + step * this.minute);
      if (rangeToInsertForce) {
        let insertIndex = slicedFilteredDayDescriptionPeriods.indexOf(rangeToInsertForce);
        slicedFilteredDayDescriptionPeriods.splice(slicedFilteredDayDescriptionPeriods.indexOf(rangeToInsertForce), 1);
        const toReplace = [];
        if (rangeToInsertForce.start < appStart) {
          toReplace.push(
            {start: rangeToInsertForce.start, end: appStart, type: rangeToInsertForce.type});
        }
        toReplace.push({start: appStart, end: appEnd, type: 'available', blocked: true , appointments: [{patientID: patientID}]});
        if (rangeToInsertForce.end > appEnd) {
          toReplace.push(
            {start: appEnd, end: rangeToInsertForce.end, type: rangeToInsertForce.type});
        }
        slicedFilteredDayDescriptionPeriods.splice(insertIndex, 1);
        toReplace.forEach(range => {
          slicedFilteredDayDescriptionPeriods.splice(insertIndex,  0, range);
          insertIndex++;
        });
      }
    }
  }

  public updateDoctorsSchedule(doctorsSelected: Array<IDoctor>, date: Date, daysToDisplay: number) {
    const copyDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    const endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + daysToDisplay);
    let result = [];
    while (copyDate < endDate) {
      const dayFormatted = this.getDoctorsFormattedDay(doctorsSelected, copyDate);
      if (dayFormatted) {
        result = result.concat(dayFormatted);
      }
      copyDate.setDate(copyDate.getDate() + 1);
    }
    return result.length > 0 ? result : null;
  }

  constructor() {
  }
}
