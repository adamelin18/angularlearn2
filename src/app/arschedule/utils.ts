export interface IBaseRange {
  start: number;
  end: number;
}
export enum modalOptions {
  details = 'Просмотреть запись',
  create = 'Создать запись',
  delete = 'Отменить запись',
  showOptions = 'showOptions',
}
export interface IButtonContainer {
  name: modalOptions;
  enabled: boolean;
}
