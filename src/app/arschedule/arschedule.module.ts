import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SidebarComponent} from './sidebar/sidebar.component';
import {PatientComponent} from './sidebar/patient/patient.component';
import {DoctorsComponent} from './sidebar/doctors/doctors.component';
import {SearchboxComponent} from './searchbox/searchbox.component';
import {DropdowOptionsListComponent} from './dropdow-options-list/dropdow-options-list.component';
import {CalendarComponent} from './calendar/calendar.component';
import {ScheduleComponent} from './schedule/schedule.component';
import {HeaderComponent} from './schedule/header/header.component';
import {ModalComponent} from './modal/modal.component';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ScheduleRootComponent} from './schedule-root/schedule-root.component';
import { TimeShortPipe } from './time-short.pipe';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    SidebarComponent,
    PatientComponent,
    DoctorsComponent,
    SearchboxComponent,
    DropdowOptionsListComponent,
    CalendarComponent,
    ScheduleComponent,
    HeaderComponent,
    ModalComponent,
    ScheduleRootComponent,
    TimeShortPipe
  ],
  exports: [
    ScheduleRootComponent
  ],
  providers: [
  ],
})
export class ArscheduleModule { }
