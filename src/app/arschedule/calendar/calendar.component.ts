import {Component, ElementRef, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {NgbDateStruct, NgbInputDatepicker} from '@ng-bootstrap/ng-bootstrap';
import {IDoctor} from '../doctor';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnChanges {
  @ViewChild('CalendarContainer') CalendarContainer: ElementRef ;
  @Input() doctors: IDoctor;
  @Input() SelectedDoctors: Array<IDoctor>;
  @Output() dateSelection: EventEmitter<Date> = new EventEmitter();
  public calendarModel: NgbDateStruct;
  public currentDate: Date;
  public maxDate: Date;
  public calendarVisible = false;
  public calendarEnabled = false;
  private selectedDate: Date;
  private static addDays(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  private dateValid(date: Date) {
    return date >= this.currentDate && date <= this.maxDate;
  }
  private dateHighlighted(date: NgbDateStruct) {
    const date_converted = this.ngbDateToDate(date);
    let res = false;
    if (this.dateValid(date_converted)) {
      this.SelectedDoctors.forEach((doctor: IDoctor) => {
        if (doctor.workdays.split(',').indexOf(String(date_converted.getDay())) >= 0) {
          res = true;
        }
      });
    }
    return res;
  }
  @HostListener('document:click', ['$event']) bgClick(event: Event) {
    if (!this.CalendarContainer.nativeElement.contains(event.target)) {
      this.discardDate();
    }
  }
  public setDate() {
    this.selectedDate = this.ngbDateToDate(this.calendarModel);
    this.dateSelection.emit(this.selectedDate);
    this.calendarVisible = false;
  }

  public discardDate() {
    this.calendarModel = this.dateToNgbDate(this.selectedDate);
    this.calendarVisible = false;
  }

  public dateToNgbDate(date: Date) {
    return {day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()};
  }

  public ngbDateToDate(date) {
    return new Date(date.year, date.month - 1, date.day);
  }

  public isDisabled = (date: NgbDateStruct) => !this.dateValid(this.ngbDateToDate(date));

  public toggleCalendar() {
    this.calendarModel = this.dateToNgbDate(this.selectedDate);
    this.calendarVisible = !this.calendarVisible;
  }

  constructor() {
    this.currentDate = new Date();
    this.selectedDate = new Date();
    this.currentDate.setHours(0, 0, 0, 0);
    this.maxDate = CalendarComponent.addDays(this.currentDate, 13);
    this.calendarModel = this.dateToNgbDate(this.currentDate);
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.calendarEnabled = this.SelectedDoctors.length !== 0;
  }
}
