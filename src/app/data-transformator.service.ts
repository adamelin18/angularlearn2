import {IAppointment, IDoctor, ITimeRange} from './doctor';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

export class DataTransformatorService {
  public DoctorsSelected: Array<IDoctor>;
  public dateSelected: Date;
  public daysToDisplay: number;
  public doctorsFormatted: Array<any>;
  private subject = new Subject<any>();

  public addAppointment(appointmentContainer) {
    const doc = this.DoctorsSelected.find((doctor: IDoctor) => {
        return doctor.id === appointmentContainer.doctorId;
    });
    let dateTime = '';
    dateTime += ('' + appointmentContainer.dateTime.getDate()).padStart(2, '0') + '.';
    dateTime += ('' + (appointmentContainer.dateTime.getMonth() + 1)).padStart(2, '0') + '.';
    dateTime += appointmentContainer.dateTime.getFullYear() + ' ';
    dateTime += ('' + appointmentContainer.dateTime.getHours()).padStart(2, '0') + ':';
    dateTime += ('' + appointmentContainer.dateTime.getMinutes()).padStart(2, '0');
    const appointment: IAppointment = {patient: appointmentContainer.patient, datetime: dateTime};
    doc.appointments = doc.appointments.concat(appointment);
    this.doctorsFormatted = this.getDoctorsFormatted();
    this.subject.next();
  }

  public deleteAppointment(appointmentContainer) {
    let dateTime = '';
    dateTime += ('' + appointmentContainer.dateTime.getDate()).padStart(2, '0') + '.';
    dateTime += ('' + (appointmentContainer.dateTime.getMonth() + 1)).padStart(2, '0') + '.';
    dateTime += appointmentContainer.dateTime.getFullYear() + ' ';
    dateTime += ('' + appointmentContainer.dateTime.getHours()).padStart(2, '0') + ':';
    dateTime += ('' + appointmentContainer.dateTime.getMinutes()).padStart(2, '0');    this.doctorsFormatted = this.getDoctorsFormatted();
    const doc = this.DoctorsSelected.find((doctor: IDoctor) => {
      return doctor.id === appointmentContainer.doctorId;
    });
    const appToDelete = doc.appointments.find(app => {
      return app.datetime === dateTime && app.patient === appointmentContainer.patientId;
    });
    doc.appointments.splice(doc.appointments.indexOf(appToDelete), 1);
    this.doctorsFormatted = this.getDoctorsFormatted();
    this.subject.next();
  }

  public getDoctors(): Observable<any> {
    return this.subject.asObservable();
  }

  private sliceDay(doctor: IDoctor, date: Date) {
    const ranges = this.periodToDateFormat(doctor.worktime[0], date);
    const result_ranges: Array<Date> = [];
    while (ranges[0] < ranges[1]) {
      const temp_date = new Date();
      temp_date.setTime(ranges[0].getTime());
      result_ranges.push(temp_date);
      ranges[0].setTime(ranges[0].getTime() + doctor.scheduleStep * 60 * 1000);
    }
    return result_ranges;
  }

  private periodToDateFormat(period: string, date: Date) {
      return period.split('-').map((point: string) => {
        const pointVal = point.split(':');
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), Number(pointVal[0]), Number(pointVal[1]));
      });
  }

  private filterIntervalsAvailableTime(slicedDay: Array<Date>, doctor: IDoctor) {
    const availableRanges: ITimeRange = doctor.timeranges.find(range => range.type === 'available');
    const slicedDayFiltered = slicedDay.filter(interval => {
      const result = availableRanges.time.some(currentRange => {
        const currentRangeDates: Array<Date> = this.periodToDateFormat(currentRange, slicedDay[0]);
        return interval < currentRangeDates[1] && interval >= currentRangeDates[0];
      });
      return result;
    });
    return slicedDayFiltered;
  }

  private addDescription(slicedFilteredDay: Array<Date>, step: number) {
    return slicedFilteredDay.map(range =>  {
      const end = new Date();
      end.setTime(range.getTime() + step * 60 * 1000);
      return {start: range, end: end, type: 'available', appointments: []};
    });
  }

  public updateDoctors(doctors: Array<IDoctor>) {
    this.DoctorsSelected = doctors;
    this.doctorsFormatted = this.getDoctorsFormatted();
    this.subject.next();
  }

  public setDaysToDisplay(days: number) {
    this.daysToDisplay = days;
    this.doctorsFormatted = this.getDoctorsFormatted();
    this.subject.next();
  }

  public updateDate(displayDate: Date): void {
    this.dateSelected = displayDate;
    this.doctorsFormatted = this.getDoctorsFormatted();
    this.subject.next();
  }

  public getDoctorsFormattedDay(displayDate: Date) {
    if (this.DoctorsSelected && this.dateSelected) {
      const result = this.DoctorsSelected
        .filter((doctor: IDoctor) => {
          return doctor.workdays.split(',').indexOf('' + displayDate.getDay()) !== -1; })
        .map((doctor: IDoctor) => {
          const tempRangesTable = this.addDescription(this.filterIntervalsAvailableTime(this.sliceDay(doctor, displayDate), doctor), doctor.scheduleStep);
          doctor.timeranges.forEach(timerange => {
            if (timerange.days.split(',').map(string => +string).indexOf(displayDate.getDay()) !== -1
              && timerange.type !== 'available') {
              timerange.time.forEach(range => {
                this.insertPeriod(tempRangesTable, range, timerange.type, doctor.scheduleStep);
              });
            }
          });
          doctor.appointments.forEach(app => {
            const datetime = app.datetime.split(' ');
            const date = datetime[0].split('.');
            const time = datetime[1].split(':');
            const dateFormatted = new Date(+date[2], +date[1] - 1, +date[0], +time[0], +time[1]);
            this.insertAppointment(tempRangesTable, dateFormatted, app.patient, doctor.scheduleStep);
          });
          return {doctor: doctor, timeRanges: tempRangesTable};
        });
      return result;
    } else {
      return null;
    }
  }

  public getDoctorsFormatted() {
    const copyDate = new Date(this.dateSelected.getFullYear(), this.dateSelected.getMonth(), this.dateSelected.getDate());
    const endDate = new Date(this.dateSelected.getFullYear(), this.dateSelected.getMonth(), this.dateSelected.getDate() + this.daysToDisplay);
    let result = [];
    while (copyDate < endDate) {
      const temp = this.getDoctorsFormattedDay(copyDate);
      if (temp) {
        result = result.concat(this.getDoctorsFormattedDay(copyDate));
      }
      copyDate.setDate(copyDate.getDate() + 1);
    }
    return result.length > 0 ? result : null;
  }

  private getRangesCrossing(X: Array<number>, Y: Array<number>) {
    const dayInMilliseconds = 86400000;
    if (Y[1] < X[0] || X[1] < Y[0]) {
      return 0;
    } else if (X[0] < Y[0] && X[1] < Y[1]) {
      return X[1] - Y[0];
    } else if (X[0] <= Y[0] && X[1] >= Y[1]) {
      return dayInMilliseconds;
      // возвращает число, заведомо большее любого интервала записи (сутки)
    } else if (Y[0] < X[0] && Y[1] < X[1]) {
      return Y[1] - X[0];
    } else if (Y[0] <= X[0] && Y[1] >= X[1]) {
      return X[1] - X[0];
    }
  }

  private insertPeriod(slicedFilteredDayDescription, range: string, type: string, step: number): void {
    const rangeDateFormat = this.periodToDateFormat(range, slicedFilteredDayDescription[0].start);
    const startAnchor = slicedFilteredDayDescription.find((slice) => {
      return slice.start >= rangeDateFormat[0];
    });
    const startIndex = slicedFilteredDayDescription.indexOf(startAnchor);
    const endAnchor = slicedFilteredDayDescription.find((slice) => {
      return slice.start >= rangeDateFormat[1] || slicedFilteredDayDescription.indexOf(slice) + 1 === slicedFilteredDayDescription.length;
    });
    const endIndex = slicedFilteredDayDescription.indexOf(endAnchor);
    for (let i = startIndex; i <= endIndex; i++) {
      if (slicedFilteredDayDescription[i]) {
        const sliceCoordinates = [
          slicedFilteredDayDescription[i].start.getTime(),
          slicedFilteredDayDescription[i].end.getTime()
        ];
        const rangeCoordinates = rangeDateFormat.map(point => point.getTime());
        if (this.getRangesCrossing(sliceCoordinates, rangeCoordinates) / (step * 1000 * 60) > 0.2) {
          slicedFilteredDayDescription.splice(i, 1);
          i--;
        }
      }
    }
    slicedFilteredDayDescription.splice(startIndex, 0, {start: rangeDateFormat[0], end: rangeDateFormat[1], type: type});

  }

  private insertAppointment(slicedFilteredDayDescriptionPeriods, appStart: Date, patientID: number, step) {
    const availableRange = slicedFilteredDayDescriptionPeriods.find(range => range.start.getTime() === appStart.getTime() && range.type === 'available');
    if (availableRange) {
      availableRange.appointments.push({patientID: patientID});
      availableRange.blocked = false;
    } else {
      const rangeToInsertForce = slicedFilteredDayDescriptionPeriods.find(range => {
        return range.type !== 'available' && appStart >= range.start && appStart <= range.end;
      });
      const appEnd = new Date();
      appEnd.setTime(appStart.getTime() + step * 60 * 1000);
      if (rangeToInsertForce) {
        let insertIndex = slicedFilteredDayDescriptionPeriods.indexOf(rangeToInsertForce);
        slicedFilteredDayDescriptionPeriods.splice(slicedFilteredDayDescriptionPeriods.indexOf(rangeToInsertForce), 1);
        const toReplace = [];
        if (rangeToInsertForce.start < appStart) {
          toReplace.push(
            {start: rangeToInsertForce.start, end: appStart, type: rangeToInsertForce.type});
        }
        toReplace.push({start: appStart, end: appEnd, type: 'available', blocked: true , appointments: [{patientID: patientID}]});
        if (rangeToInsertForce.end > appEnd) {
          toReplace.push(
            {start: appEnd, end: rangeToInsertForce.end, type: rangeToInsertForce.type});
        }
        slicedFilteredDayDescriptionPeriods.splice(insertIndex, 1);
        toReplace.forEach(range => {
          slicedFilteredDayDescriptionPeriods.splice(insertIndex,  0, range);
          insertIndex++;
        });
      }
    }
  }

  constructor() {
  }
}
